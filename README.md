# Dynamic BackstopJS Generator

The main goal of the Dynamic BackstopJS Generator module is to generate a backstop.json file dynamically depending on the testing scenario.

## Quick start

In order to generate a basic `backstop.json` file just visit route `/dynamic-backstopjs-generator/backstop.json`.
It will generate scenarios for front page comparison.

One of the default plugins you can use without any configuration or coding is `random_nodes`.
You can automatically generate a set of random pages to be compared by BackstopJS.

In order to use any plugin you have to pass a `type` parameter to your query.
In the next example we'll generate 10 random nodes for comparison.
Route: `/dynamic-backstopjs-generator/backstop.json?type=random_nodes&limit=10`.

Now, let's add the site URL which we will use as a reference. Just add `base_referenceUrl` parameter to your query.

So, in order to get `backstop.json` which will generate 10 random nodes and compare them with your reference site you need to enter:
`/dynamic-backstopjs-generator/backstop.json?type=random_nodes&limit=10&base_referenceUrl=http://example.com`

Congratulations! You can use your `backstop.json` file in your QA process.

## Configuration

Using configuration form you can provide various view ports and basic scenario settings like `hideSelectors` or `removeSelectors`.

## Advanced Settings

Please, note any BackstopJS configuration variable could be overridden via query.
Also, you can use `POST` requests with JSON data if you need to put complex data in your query.

Happy testing!
