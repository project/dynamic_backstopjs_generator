<?php

namespace Drupal\dynamic_backstopjs_generator;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Helper for generate backstop json object.
 */
class Generator {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a Generator object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Generate basic backstop object.
   */
  public function generateBacksopJson(array $options = []) {
    $default = $this->configFactory->get('dynamic_backstopjs_generator.settings')->getRawData();
    unset($default['_core']);
    unset($default['scenarios']);
    $allowed = array_keys($default);
    $allowed[] = 'ci';
    $allowed[] = 'report';
    $allowed[] = 'cookiePath';
    $allowed[] = 'onBeforeScript';
    $filtered = array_filter(
      $options,
      function ($key) use ($allowed) {
          return in_array($key, $allowed);
      },
      ARRAY_FILTER_USE_KEY
    );
    $result = array_merge($default, $filtered);
    return $result;
  }

  /**
   * Generate scenario for node or path.
   */
  public function generateScenario(array $options = []) {
    if (isset($options['node'])) {
      /** @var \Drupal\node\Entity\Node $node */
      $node = $options['node'];
      $options['path'] = $node->toUrl()->toString();
      $label = $node->getTitle() . ' | ' . $node->id();
    }
    else {
      $label = isset($options['label']) ? $options['label'] : $options['path'];
    }
    if (!isset($options['base_url'])) {
      $options['base_url'] = \Drupal::request()->getSchemeAndHttpHost();
    }
    if (!isset($options['base_referenceUrl'])) {
      $options['base_referenceUrl'] = \Drupal::request()->getSchemeAndHttpHost();
    }
    // Convert delay to integer.
    if (isset($options['delay'])) {
      $options['delay'] = (int) $options['delay'];
    }
    // Convert misMatchThreshold to float.
    if (isset($options['misMatchThreshold'])) {
      $options['misMatchThreshold'] = (float) $options['misMatchThreshold'];
    }
    $default = $this->configFactory->get('dynamic_backstopjs_generator.settings')->get('scenarios');
    $default['label'] = $label;
    $default['url'] = $options['base_url'] . $options['path'];
    $default['referenceUrl'] = $options['base_referenceUrl'] . $options['path'];
    // For more info see https://github.com/garris/BackstopJS#advanced-scenarios
    $allowed = [
      'delay',
      'hideSelectors',
      'removeSelectors',
      'misMatchThreshold',
      'selectors',
      'selectorExpansion',
      'expect',
      'keyPressSelectors',
      'clickSelectors',
      'hoverSelectors',
      'readySelector',
      'hoverSelector',
      'clickSelector',
      'postInteractionWait',
      'scrollToSelector',
      'requireSameDimensions',
      'cookiePath'
    ];
    $filtered = array_filter(
      $options,
      function ($key) use ($allowed) {
          return in_array($key, $allowed);
      },
      ARRAY_FILTER_USE_KEY
    );
    $result = array_merge($default, $filtered);
    return $result;
  }

}
