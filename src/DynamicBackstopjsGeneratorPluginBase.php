<?php

namespace Drupal\dynamic_backstopjs_generator;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for dynamic_backstopjs_generator plugins.
 */
abstract class DynamicBackstopjsGeneratorPluginBase extends PluginBase implements DynamicBackstopjsGeneratorInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The backstop json object generator.
   *
   * @var \Drupal\dynamic_backstopjs_generator\Generator
   */
  protected $generator;

  /**
   * ConfigFactory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $conginFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('dynamic_backstopjs_generator'),
      $container->get('config.factory')
    );
  }

  /**
   * Constructs a DynamicBackstopjsGeneratorPluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Entity Type Manager.
   * @param \Drupal\dynamic_backstopjs_generator\Generator $generator
   *   The backstop json object generator.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config Factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, Generator $generator, ConfigFactoryInterface $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->generator = $generator;
    $this->conginFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function generate(array $options) {
    return [];
  }

}
