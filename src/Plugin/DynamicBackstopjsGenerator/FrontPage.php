<?php

namespace Drupal\dynamic_backstopjs_generator\Plugin\DynamicBackstopjsGenerator;

use Drupal\dynamic_backstopjs_generator\DynamicBackstopjsGeneratorPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the dynamic_backstopjs_generator.
 *
 * @DynamicBackstopjsGenerator(
 *   id = "front_page",
 *   label = @Translation("Front Page"),
 *   description = @Translation("Generate backstop.json for front page.")
 * )
 */
class FrontPage extends DynamicBackstopjsGeneratorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function generate(array $options) {
    $options['id'] = 'front_page';
    $options['path'] = '/';
    $options['label'] = 'Test Front Page';
    $backstop = $this->generator->generateBacksopJson($options);
    $scenario = $this->generator->generateScenario($options);
    $backstop['scenarios'] = [$scenario];
    return $backstop;
  }

}
