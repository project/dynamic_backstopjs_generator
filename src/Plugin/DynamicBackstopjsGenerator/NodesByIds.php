<?php

namespace Drupal\dynamic_backstopjs_generator\Plugin\DynamicBackstopjsGenerator;

use Drupal\dynamic_backstopjs_generator\DynamicBackstopjsGeneratorPluginBase;

/**
 * Plugin implementation of the dynamic_backstopjs_generator.
 *
 * @DynamicBackstopjsGenerator(
 *   id = "nodes_by_ids",
 *   label = @Translation("Generate backstop json for nodes by node id"),
 *   description = @Translation("Generate backstop json nodes by node id.")
 * )
 */
class NodesByIds extends DynamicBackstopjsGeneratorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function generate(array $options) {
    $options['id'] = 'nodes_by_ids';
    if (!isset($options['ids'])) {
      return ['msg' => 'Please set ids query param with array of nodes id'];
    }
    $ids = $options['ids'];
    if (gettype($ids) == 'string') {
      $ids = explode(',', $options['ids']);
    }
    $backstop = $this->generator->generateBacksopJson($options);
    $storage = $this->entityTypeManager->getStorage('node');
    $scenarios = [];
    if (!empty($ids)) {
      foreach ($ids as $id) {
        $options['node'] = $storage->load($id);
        $scenarios[] = $this->generator->generateScenario($options);
      }
    }
    $backstop['scenarios'] = $scenarios;
    return $backstop;
  }

}
