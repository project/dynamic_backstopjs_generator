<?php

namespace Drupal\dynamic_backstopjs_generator\Plugin\DynamicBackstopjsGenerator;

use Drupal\dynamic_backstopjs_generator\DynamicBackstopjsGeneratorPluginBase;

/**
 * Plugin implementation of the dynamic_backstopjs_generator.
 *
 * @DynamicBackstopjsGenerator(
 *   id = "list_paths",
 *   label = @Translation("Generate backstop json from url list"),
 *   description = @Translation("Generate backstop json from url list.")
 * )
 */
class ListPaths extends DynamicBackstopjsGeneratorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function generate(array $options) {
    $options['id'] = 'list_paths';
    $backstop = $this->generator->generateBacksopJson($options);
    $paths = $options['urls'];
    $scenarios = [];
    if (!empty($paths)) {
      foreach ($paths as $path) {
        $options['path'] = $path;
        $scenarios[] = $this->generator->generateScenario($options);
      }
    }
    $backstop['scenarios'] = $scenarios;
    return $backstop;
  }

}
