<?php

namespace Drupal\dynamic_backstopjs_generator\Plugin\DynamicBackstopjsGenerator;

use Drupal\dynamic_backstopjs_generator\DynamicBackstopjsGeneratorPluginBase;

/**
 * Plugin implementation of the dynamic_backstopjs_generator.
 *
 * @DynamicBackstopjsGenerator(
 *   id = "random_nodes",
 *   label = @Translation("Generate backstop json for random nodes"),
 *   description = @Translation("Generate backstop json for random nodes.")
 * )
 */
class RandomNodes extends DynamicBackstopjsGeneratorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function generate(array $options) {
    $options['id'] = 'random_nodes';
    $backstop = $this->generator->generateBacksopJson($options);

    $storage = $this->entityTypeManager->getStorage('node');
    $query = $storage->getQuery();
    $query
      ->condition('status', 1)
      ->addTag('random')
      ->range(0, isset($options['limit']) ? $options['limit'] : 10);
    $ids = $query->execute();
    $scenarios = [];
    if (!empty($ids)) {
      foreach ($ids as $id) {
        $options['node'] = $storage->load($id);
        $scenarios[] = $this->generator->generateScenario($options);
      }
    }
    $backstop['scenarios'] = $scenarios;
    return $backstop;
  }

}
