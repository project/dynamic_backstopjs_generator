<?php

namespace Drupal\dynamic_backstopjs_generator;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface for dynamic_backstopjs_generator plugins.
 */
interface DynamicBackstopjsGeneratorInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Returns the backstop json object.
   *
   * @param array $options
   *   Options for Generate backstop object.
   *
   * @return array
   *   Return array for generate backstop.json file.
   */
  public function generate(array $options);

}
