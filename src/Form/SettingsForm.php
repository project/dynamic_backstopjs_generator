<?php

namespace Drupal\dynamic_backstopjs_generator\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Dynamic BackstopJS Generator settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dynamic_backstopjs_generator_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['dynamic_backstopjs_generator.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $viewports = $form_state->get('viewports');
    if (empty($viewports)) {
      $viewports = $this->config('dynamic_backstopjs_generator.settings')->get('viewports');
      $form_state->set('viewports', $viewports);
    }

    // Example https://git.drupalcode.org/project/examples/-/blob/3.x/modules/form_api_example/src/Form/AjaxAddMore.php
    $form['#tree'] = TRUE;
    $form['viewports_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Viewports'),
      '#prefix' => '<div id="viewports-fieldset-wrapper">',
      '#suffix' => '</div>',
      '#description' => $this->t('List of screen size objects your DOM will be tested against.'),
    ];
    foreach ($viewports as $viewport) {
      $form['viewports_fieldset']['viewports'][] = $this->viewportElement($viewport);
    }
    $form['viewports_fieldset']['actions'] = [
      '#type' => 'actions',
    ];
    $form['viewports_fieldset']['actions']['add_viewport'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add viewport'),
      '#submit' => ['::addViewportCallback'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'viewports-fieldset-wrapper',
      ],
    ];
    if (!empty($viewports)) {
      $form['viewports_fieldset']['actions']['remove_viewport'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove viewport'),
        '#submit' => ['::removeViewportCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'viewports-fieldset-wrapper',
        ],
      ];
    }
    $scenarios = $this->config('dynamic_backstopjs_generator.settings')->get('scenarios');
    $form['scenarios_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Advanced Scenarios'),
    ];
    $form['scenarios_fieldset']['delay'] = [
      '#type' => 'number',
      '#title' => $this->t('Delay'),
      '#default_value' => $scenarios['delay'],
      '#description' => $this->t('Wait for x milliseconds'),
    ];
    $form['scenarios_fieldset']['misMatchThreshold'] = [
      '#type' => 'number',
      '#min' => 0,
      '#step' => 0.01,
      '#title' => $this->t('misMatchThreshold'),
      '#default_value' => $scenarios['misMatchThreshold'],
      '#description' => $this->t('Percentage of different pixels allowed to pass test.'),
    ];
    $form['scenarios_fieldset']['hideSelectors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('hideSelectors'),
      '#default_value' => implode(PHP_EOL, $scenarios['hideSelectors']),
      '#description' => $this->t('Selectors set to visibility: hidden. Use new line as a delimiter for multiple selectors.'),
    ];
    $form['scenarios_fieldset']['removeSelectors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('removeSelectors'),
      '#default_value' => implode(PHP_EOL, $scenarios['removeSelectors']),
      '#description' => $this->t('Selectors set to display: none. Use new line as a delimiter for multiple selectors.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Callback for both ajax-viewports buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['viewports_fieldset'];
  }

  /**
   * Add new viewport to form.
   */
  public function addViewportCallback(array &$form, FormStateInterface $form_state) {
    $viewports = $form_state->get('viewports');
    $viewports[] = [
      'label' => 'new',
      'width' => 1280,
      'height' => 720,
    ];
    $form_state->set('viewports', $viewports);
    $form_state->setRebuild();
  }

  /**
   * Remove last viewport from form.
   */
  public function removeViewportCallback(array &$form, FormStateInterface $form_state) {
    $viewports = $form_state->get('viewports');
    array_pop($viewports);
    $form_state->set('viewports', $viewports);
    $form_state->setRebuild();
  }

  /**
   * Viewport element.
   */
  private function viewportElement($viewport) {
    return [
      '#type' => 'fieldgroup',
      'label' => [
        '#type' => 'textfield',
        '#title' => $this->t('Label'),
        '#default_value' => $viewport['label'],
      ],
      'width' => [
        '#type' => 'textfield',
        '#title' => $this->t('Width'),
        '#default_value' => $viewport['width'],
      ],
      'height' => [
        '#type' => 'textfield',
        '#title' => $this->t('Height'),
        '#default_value' => $viewport['height'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $scenarios = $form_state->getValue('scenarios_fieldset');
    $scenarios['hideSelectors'] = preg_split('/\r\n|\r|\n/', $scenarios['hideSelectors']);
    $scenarios['removeSelectors'] = preg_split('/\r\n|\r|\n/', $scenarios['removeSelectors']);
    $this->config('dynamic_backstopjs_generator.settings')
      ->set('viewports', $form_state->get('viewports'))
      ->set('scenarios', $scenarios)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
