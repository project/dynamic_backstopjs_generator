<?php

namespace Drupal\dynamic_backstopjs_generator\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\dynamic_backstopjs_generator\DynamicBackstopjsGeneratorInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\dynamic_backstopjs_generator\DynamicBackstopjsGeneratorPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Returns responses for Dynamic BackstopJS Generator routes.
 */
class DynamicBackstopjsGeneratorController extends ControllerBase {

  /**
   * The Plugin Manager.
   *
   * @var \Drupal\dynamic_backstopjs_generator\DynamicBackstopjsGeneratorPluginManager
   */
  protected $pluginManager;

  /**
   * The controller constructor.
   *
   * @param \Drupal\dynamic_backstopjs_generator\DynamicBackstopjsGeneratorPluginManager $pluginManager
   *   The plugin.manager.archiver service.
   */
  public function __construct(DynamicBackstopjsGeneratorPluginManager $pluginManager) {
    $this->pluginManager = $pluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.dynamic_backstopjs_generator')
    );
  }

  /**
   * Builds the response.
   */
  public function build(Request $request) {
    $backstop = [];
    $params = ['type' => 'front_page'];
    if ($request->isMethod('GET')) {
      $params = array_merge($params, $request->query->all());
    }
    if ($request->isMethod('POST')) {
      $params = array_merge($params, json_decode($request->getContent(), TRUE));
    }

    if (!$this->pluginManager->hasDefinition($params['type'])) {
      return new JsonResponse(['msg' => 'Plugin not found'], 403);
    }
    $plugin = $this->pluginManager->createInstance($params['type']);
    $backstop = $plugin->generate($params);
    $response = new JsonResponse($backstop);
    return $response;
  }

}
