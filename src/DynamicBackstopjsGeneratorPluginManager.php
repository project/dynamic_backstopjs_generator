<?php

namespace Drupal\dynamic_backstopjs_generator;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * DynamicBackstopjsGenerator plugin manager.
 */
class DynamicBackstopjsGeneratorPluginManager extends DefaultPluginManager {

  /**
   * Constructs DynamicBackstopjsGeneratorPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/DynamicBackstopjsGenerator',
      $namespaces,
      $module_handler,
      'Drupal\dynamic_backstopjs_generator\DynamicBackstopjsGeneratorInterface',
      'Drupal\dynamic_backstopjs_generator\Annotation\DynamicBackstopjsGenerator'
    );
    $this->alterInfo('dynamic_backstopjs_generator_info');
    $this->setCacheBackend($cache_backend, 'dynamic_backstopjs_generator_plugins');
  }

}
